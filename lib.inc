%define SYS_READ        0
%define SYS_WRITE       1
%define SYS_EXIT        60

%define FD_STDOUT       1
%define FD_STDIN        0

%define ASCII_SPACE     0x20
%define ASCII_TAB       0x9
%define ASCII_NEW_LINE  0xA

section .text

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; rdi - указатель на строку
; rcx - номер текущего символа (после конца цикла будет равен длине строки + 1) 
; rax - текущий символ
string_length:
    xor     rcx, rcx
    .loop:
    mov     al, byte[rdi + rcx]
    inc     rcx
    test    al, al
    jnz     .loop
    mov     rax, rcx
    dec     rax
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push    rdi
    call    string_length
    mov     rdx, rax
    mov     rax, SYS_WRITE
    pop     rsi
    mov     rdi, FD_STDOUT
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov     rdi, ASCII_NEW_LINE

; Принимает код символа и выводит его в stdout
print_char:
    push    rdi ; [rsp] = code
    mov     rax, SYS_WRITE
    mov     rdi, FD_STDOUT
    mov     rsi, rsp
    mov     rdx, 1
    syscall
    pop     rdi
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test    rdi, rdi
    jnl     .print
    push    rdi
    mov     rdi, '-'
    call    print_char
    pop     rdi
    neg     rdi
    .print:

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push    rbx
    mov     rbx, rsp
    mov     rax, rdi
    mov     rcx, 10
    dec     rsp                 ; one does not simply
    mov     byte[rsp], 0        ; push byte on stack
.loop:
    xor     rdx, rdx
    div     rcx
    add     dl, '0'
    dec     rsp
    mov     byte[rsp], dl
    test     rax, rax
    jnz     .loop
    mov     rdi, rsp
    call    print_string
    mov     rsp, rbx
    pop     rbx
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; rdi - указатель на первую строку
; rsi - указатель на вторую строку
; rcx - offset
string_equals:
    xor     rcx, rcx
    .loop:
    mov     al, [rdi + rcx]
    cmp     al, byte[rsi + rcx]
    jne     .not_equal
    test    al, al
    je      .equal
    inc     rcx
    jmp     .loop
    .equal:
    mov     rax, 1
    ret
    .not_equal:
    mov     rax, 0
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov     rax, SYS_READ
    mov     rdi, FD_STDIN
    mov     rdx, 1          ; read 1 byte
    dec     rsp             ; allocate 1 byte
    mov     rsi, rsp
    syscall
    test     al, al
    jz      .exit
    mov     al, [rsp]
    .exit:
    inc     rsp
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

; rdi - address
; rsi - size
read_word:
    push    rbx
    push    r12
    push    r13
    mov     rbx, rdi ; rbx = address
    mov     r12, rsi ; r12 = buffer length
    xor     r13, r13 ; r13 = word length = 0
    dec     r12 ; сохраняем 1 байт для NULL-терминатора
    .loop:
    cmp     r13, r12 ; length < size
    jb      .loop_body
    jmp     .fail
    .loop_body:
    call    read_char
    cmp     al, ASCII_SPACE
    je      .spacing
    cmp     al, ASCII_TAB
    je      .spacing
    cmp     al, ASCII_NEW_LINE
    je      .spacing
    cmp     al, 0
    je      .success
    mov     byte[rbx + r13], al
    inc     r13
    jmp     .loop
    .spacing:
    test     r13, r13
    jnz      .success
    jmp     .loop
    .success:
    mov     byte[rbx + r13], 0
    mov     rax, rbx
    mov     rdx, r13
    jmp     .return
    .fail:
    mov     rax, 0
    .return:
    pop     r13
    pop     r12
    pop     rbx
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
; rdi - указатель на строку
parse_int:
    xor     rax, rax
    xor     rdx, rdx
    cmp     byte[rdi], '-'
    je      .signed
    cmp     byte[rdi], '+'
    jne     .unsigned
    inc     rdi
    jmp     .unsigned
    .signed:
    inc     rdi
    call    parse_uint
    neg     rax
    inc     rdx
    ret 
    .unsigned:
    ; tail call optimization

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; rdi - указатель на строку
parse_uint:
    push    rbx
    push    r12
    xor     rax, rax ; number
    xor     r12, r12 ; length
    mov     rbx, 10  ; system base = 10
    xor     rdx, rdx ; для корректного умножение
    .loop:
    movzx   rcx,  byte[rdi + r12]    ; current symbol
    test    rcx,  rcx
    jz      .success
    cmp     rcx,  '0'
    jb      .success
    cmp     rcx,  '9'
    ja      .success
    sub     rcx,  '0'
    mul     rbx
    add     rax, rcx
    inc     r12
    jmp     .loop
    .success:
    mov     rdx, r12
    pop     r12
    pop     rbx
    ret

; Принимает указатель на null-terminated строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi - указатель на строку
; rsi - укзаатель на буфер
; rdx - длина буфера
string_copy:
    xor     rax, rax
    .loop:
    cmp     rax, rdx
    jb      .loop_body
    jmp     .fail
    .loop_body:
    mov     cl, byte[rdi + rax] 
    mov     byte[rsi + rax], cl
    inc     rax
    test    cl, cl
    jz      .success
    jmp     .loop
    .fail:
    xor     rax, rax
    .success:
    ret

